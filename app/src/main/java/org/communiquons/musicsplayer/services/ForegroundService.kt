package org.communiquons.musicsplayer.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import org.communiquons.musicsplayer.R
import org.communiquons.musicsplayer.activities.MainActivity
import org.communiquons.musicsplayer.entities.MusicEntry
import org.communiquons.musicsplayer.getConfig
import org.communiquons.musicsplayer.helpers.MusicCacheCallback
import org.communiquons.musicsplayer.helpers.downloadMusic
import org.communiquons.musicsplayer.helpers.getCachedList
import org.communiquons.musicsplayer.receivers.PlayerActionReceiver
import org.communiquons.musicsplayer.receivers.PlayerActionReceiver.Companion.ACTION_NEXT
import org.communiquons.musicsplayer.receivers.PlayerActionReceiver.Companion.ACTION_PAUSE
import org.json.JSONObject
import java.io.File


/**
 * Main foreground service of the application
 *
 * Based on https://androidwave.com/foreground-service-android-example/
 *
 * @author Pierre HUBERT
 */
class ForegroundService : Service(), MusicCacheCallback {

    companion object {

        private const val CHANNEL_ID = "ForegroundServiceChannel"
        const val EXTRA_MUSIC_ENTRY = "music_entry"

        const val NOTIFY_ACTION = "SERVICE_UPDATE"
        const val EXTRA_STATE = "state"

        private var instance: ForegroundService? = null

        fun Pause() {
            instance?.apply {
                pauseMusic()
            }
        }

        fun ChangeMusic(entry: MusicEntry) {
            instance?.apply {
                playMusic(entry)
            }
        }

        fun Resume() {
            instance?.apply {
                resumeMusic()
            }
        }

        fun CurrEntry(): MusicEntry? {
            instance?.apply {
                return currEntry
            }
            return null
        }

        fun NextMusic() {
            instance?.apply {
                openNext()
            }
        }

        fun CurrState(): NewState {
            instance?.apply {
                return currState
            }

            return NewState.IDLE
        }

        fun CurrProgress(): Int {
            instance?.apply {
                return musicProgress()
            }
            return 0
        }
    }

    enum class NewState(val value: Int) {
        IDLE(0),
        DOWNLOADING(1),
        PLAYING(2),
        PAUSED(3);

        companion object {
            private val values = values()
            fun getByValue(value: Int) = values.firstOrNull { it.value == value }
        }
    }


    val config by lazy {
        getConfig(this)
    }

    var currEntry: MusicEntry? = null
    var currState: NewState = NewState.IDLE
    var mediaPlayer: MediaPlayer? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val musicEntry = MusicEntry(JSONObject(intent!!.getStringExtra(EXTRA_MUSIC_ENTRY)!!))

        createNotificationChannel()

        playMusic(musicEntry)


        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    /**
     * Create a notification channel
     */
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager =
                getSystemService(NotificationManager::class.java)!!
            manager.createNotificationChannel(serviceChannel)
        }
    }

    private fun playMusic(entry: MusicEntry) {

        // Stop previous player
        mediaPlayer?.apply {
            stop()
        }

        currEntry = entry

        // Show the notification

        // Open app
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        // Pause
        val pauseIntent = Intent(this, PlayerActionReceiver::class.java).apply {
            action = ACTION_PAUSE
        }
        val pausePendingIntent = PendingIntent.getBroadcast(this, 0, pauseIntent, 0)


        // Next music
        val nextIntent = Intent(this, PlayerActionReceiver::class.java).apply {
            action = ACTION_NEXT
        }
        val nextPendingIntent = PendingIntent.getBroadcast(this, 0, nextIntent, 0)


        // Push notification
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("MusicsPlayer")
            .setContentText(entry.fullName)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_music)
            .addAction(R.drawable.ic_launcher_background, "Pause", pausePendingIntent)
            .addAction(R.drawable.ic_launcher_background, "Next", nextPendingIntent)
            .build()

        startForeground(1, notification)

        // Download the music (if required)
        notify(NewState.DOWNLOADING)
        downloadMusic(config, entry, this)
    }

    override fun downloadError() {
        Log.e("ForegroundService", "Could not download the music!")
        openNext()
    }

    override fun downloadSuccess(file: File) {
        Log.v(ForegroundService::class.java.simpleName, "Downloaded music, starting to play...")

        mediaPlayer = MediaPlayer().apply {
            setAudioAttributes(AudioAttributes.Builder().setLegacyStreamType(AudioManager.STREAM_MUSIC).build())
            setDataSource(applicationContext, Uri.fromFile(file))
            prepare()
            start()

            // Play next music when the end of the current one has been reached
            setOnCompletionListener { openNext() }
        }

        notify(NewState.PLAYING)
    }

    /**
     * Get current music progress
     */
    fun musicProgress(): Int {
        mediaPlayer?.apply {
            return currentPosition * 100 / duration
        }
        return 0
    }

    fun pauseMusic() {
        mediaPlayer?.apply {
            if (!isPlaying) {
                start()
                notify(NewState.PLAYING)
            } else {
                pause()
                notify(NewState.PAUSED)
            }
        }
    }

    fun resumeMusic() {
        mediaPlayer?.apply {
            if (!isPlaying) {
                start()
                notify(NewState.PLAYING)
            }
        }
    }

    fun openNext() {
        playMusic(getCachedList(config).random())
    }

    private fun notify(newState: NewState) {
        currState = newState
        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(NOTIFY_ACTION).apply {
            putExtra(EXTRA_STATE, newState.value)
        })
    }
}