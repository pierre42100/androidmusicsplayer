package org.communiquons.musicsplayer.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import org.communiquons.musicsplayer.services.ForegroundService

class PlayerActionReceiver : BroadcastReceiver() {

    companion object {
        const val ACTION_PAUSE = "PAUSE"
        const val ACTION_NEXT = "NEXT"
    }

    override fun onReceive(context: Context?, intent: Intent?) {

        if (intent == null)
            return

        Log.v(PlayerActionReceiver::class.java.simpleName, "Received action: ${intent.action}")

        when (intent.action) {
            ACTION_PAUSE -> ForegroundService.Pause()
            ACTION_NEXT -> ForegroundService.NextMusic()
        }
    }

}