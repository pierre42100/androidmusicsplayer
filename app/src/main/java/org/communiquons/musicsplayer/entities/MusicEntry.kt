package org.communiquons.musicsplayer.entities

import org.json.JSONObject

class MusicEntry(val id: Int, val artist: String, val title: String) {

    constructor(obj: JSONObject) : this(
        obj.getInt("id"),
        obj.getString("artist"),
        obj.getString("title")
    )

    val fullName: String
        get() = "$artist - $title"

    /**
     * Serialize this [MusicEntry] as a [JSONObject]
     */
    fun serialize(): JSONObject {
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("artist", artist)
        obj.put("title", title)
        return obj
    }
}