package org.communiquons.musicsplayer

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import org.communiquons.musicsplayer.entities.MusicEntry
import java.io.File
import java.lang.RuntimeException

class Config(context: Context) {

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    /**
     * The file where the list of musics will be cached
     */
    val listCacheFile = File(context.filesDir, "musics_list")

    /**
     * The directory where musics will be stored
     */
    val musicsCacheDir = File(context.filesDir, "musics_cache")

    /**
     * Check out whether the application is correctly setup or not
     */
    val isSetup: Boolean
        get() = serverURL != null && serverToken != null

    init {
        if(!musicsCacheDir.exists() && !musicsCacheDir.mkdirs())
            throw RuntimeException("Could not create musics cache dir ($musicsCacheDir)!")
    }

    /**
     * Server URL
     */
    var serverURL: String?
        get() = prefs.getString("serverURL", null)
        set(value) {
            prefs.edit().putString("serverURL", value).apply()
        }

    /**
     * Server token
     */
    var serverToken: String?
        get() = prefs.getString("serverToken", null)
        set(value) {
            prefs.edit().putString("serverToken", value).apply()
        }


    /**
     * Get the path to the cached version of a music
     */
    fun musicFile(m: MusicEntry): File {
        return File(musicsCacheDir, "${m.id}.mp3")
    }
}

private var config: Config? = null

/**
 * Get the current configuration of the application
 *
 * @param context The context of the application
 * @return The instance of the config object
 */
fun getConfig(context: Context): Config {

    if (config == null)
        config = Config(context)

    return config!!
}