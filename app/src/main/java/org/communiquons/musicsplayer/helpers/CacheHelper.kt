package org.communiquons.musicsplayer.helpers

import com.android.volley.*
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.NoCache
import org.communiquons.musicsplayer.Config
import org.communiquons.musicsplayer.entities.MusicEntry
import java.io.File


/**
 * Musics cache helper
 *
 * @author Pierre HUBERT
 */

interface MusicCacheCallback {
    fun downloadError()
    fun downloadSuccess(file: File)
}

private val requestQueue by lazy {
    val queue = RequestQueue(NoCache(), BasicNetwork(HurlStack()))
    queue.start()
    queue
}

/**
 * Download a music or return the cached version of a music, if available
 */
fun downloadMusic(conf: Config, m: MusicEntry, cb: MusicCacheCallback) {

    val file = conf.musicFile(m)

    // Check if the file already exists
    if (file.exists()) {
        cb.downloadSuccess(file)
        return
    }

    requestQueue.add(VolleySaveRequest(conf, file, cb, m))
}


/**
 * My custom request written to save the music once it is downloaded
 */
private class VolleySaveRequest(
    val conf: Config,
    val target: File,
    val cb: MusicCacheCallback,
    m: MusicEntry
) :
    Request<Boolean>(Method.GET, "${conf.serverURL}download/${m.id}", null) {

    override fun getHeaders(): MutableMap<String, String> {
        val headers = HashMap<String, String>()
        headers["Token"] = conf.serverToken!!
        return headers
    }

    override fun parseNetworkResponse(response: NetworkResponse?): Response<Boolean> {
        return try {
            target.writeBytes(response!!.data)
            Response.success(true, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: Exception) {
            e.printStackTrace()
            Response.error(VolleyError(e))
        }
    }

    override fun deliverResponse(response: Boolean?) {
        if (response == true)
            cb.downloadSuccess(target)
        else
            cb.downloadError()
    }

    override fun deliverError(error: VolleyError?) {
        cb.downloadError()
    }
}