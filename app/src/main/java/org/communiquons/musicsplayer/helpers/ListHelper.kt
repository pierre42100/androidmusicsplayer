package org.communiquons.musicsplayer.helpers

import android.util.Log
import com.android.volley.Request.Method.GET
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.NoCache
import org.communiquons.musicsplayer.Config
import org.communiquons.musicsplayer.entities.MusicEntry
import org.json.JSONArray

interface GetListCallback {
    fun onGotList()
    fun onError()
}

private val requestQueue by lazy {
    val queue = RequestQueue(NoCache(), BasicNetwork(HurlStack()))
    queue.start()
    queue
}

/**
 * Fetch the list of musics from the server
 */
fun refreshListMusics(config: Config, listener: GetListCallback) {

    if (!config.isSetup) {
        Log.e("ListHelper", "Attempting to refresh list of music with incomplete configuration!")
        listener.onError()
        return
    }

    // Create & run the request
    requestQueue.add(
        object : JsonArrayRequest(
            GET,
            config.serverURL + "list",

            null, Response.Listener<JSONArray> {

                // Parse the response
                val list = ArrayList<MusicEntry>()

                for (num in 0 until it.length()) {
                    val obj = it.getJSONObject(num)
                    list.add(
                        MusicEntry(
                            obj.getInt("id"),
                            obj.getString("artist"),
                            obj.getString("title")
                        )
                    )
                }

                cacheList(config, list)

                listener.onGotList()

            },

            // In case of error
            Response.ErrorListener { listener.onError() }
        ) {
            override fun getHeaders(): MutableMap<String, String> {

                val map = HashMap<String, String>()
                map["token"] = config.serverToken!! // Add authentication token
                return map

            }
        }
    )

}


/**
 * Cache (serialize) the list of musics
 */
private fun cacheList(config: Config, list: List<MusicEntry>) {
    val out = JSONArray()
    list.sortedBy { it -> it.fullName }.map { it -> it.serialize() }.forEach { it -> out.put(it) }
    config.listCacheFile.writeText(out.toString())
}

/**
 * Get the cached version of the list
 */
fun getCachedList(config: Config): List<MusicEntry> {
    if (!config.listCacheFile.exists()) {
        Log.w("ListHelper", "The list of music was not found, returning an empty one...")
        return ArrayList()
    }

    val text = config.listCacheFile.readText()
    val array = JSONArray(text)
    val list = ArrayList<MusicEntry>()
    for (i in 0 until array.length()) {
        list.add(MusicEntry(array.getJSONObject(i)))
    }

    return list
}