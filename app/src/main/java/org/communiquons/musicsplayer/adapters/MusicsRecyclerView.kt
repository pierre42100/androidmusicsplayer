package org.communiquons.musicsplayer.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.communiquons.musicsplayer.R
import org.communiquons.musicsplayer.entities.MusicEntry

interface MusicRecyclerViewListener {
    fun onMusicClick(m: MusicEntry)
}

class MusicsRecyclerView(var list: List<MusicEntry>, val listener: MusicRecyclerViewListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MusicsViewHolder(parent.context, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MusicsViewHolder).bindView(list[position])
    }

    private inner class MusicsViewHolder(context: Context, viewGroup: ViewGroup) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.music_item,
                viewGroup,
                false
            )
        ) {

        init {
            itemView.setOnClickListener {
                listener.onMusicClick(list[adapterPosition])
            }
        }

        fun bindView(music: MusicEntry) {
            itemView.findViewById<TextView>(R.id.title).text = music.title
            itemView.findViewById<TextView>(R.id.artist).text = music.artist
        }

    }
}