package org.communiquons.musicsplayer.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.communiquons.musicsplayer.R
import org.communiquons.musicsplayer.adapters.MusicRecyclerViewListener
import org.communiquons.musicsplayer.adapters.MusicsRecyclerView
import org.communiquons.musicsplayer.entities.MusicEntry
import org.communiquons.musicsplayer.helpers.GetListCallback
import org.communiquons.musicsplayer.helpers.getCachedList
import org.communiquons.musicsplayer.helpers.refreshListMusics
import org.communiquons.musicsplayer.services.ForegroundService
import org.communiquons.musicsplayer.services.ForegroundService.Companion.EXTRA_STATE
import org.communiquons.musicsplayer.services.ForegroundService.Companion.NOTIFY_ACTION
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : BaseActivity(), MusicRecyclerViewListener {

    private val adapter: MusicsRecyclerView by lazy {
        MusicsRecyclerView(ArrayList(), this)
    }

    private var playerStatus: ForegroundService.NewState = ForegroundService.CurrState()

    private var currList: List<MusicEntry> = Collections.emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Register receiver for music state update
        LocalBroadcastManager.getInstance(this).registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                playerStatus =
                    ForegroundService.NewState.getByValue(intent!!.getIntExtra(EXTRA_STATE, 0))!!
                refreshStatus()
            }
        }, IntentFilter(NOTIFY_ACTION))


        // Check if the application is setup
        if (!config.isSetup) {
            startActivity(Intent(this, SetupActivity::class.java))
        }


        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapter

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            musicProgress.min = 0
        }
        musicProgress.max = 100

        play_button.setOnClickListener { play(null) }
        pause_button.setOnClickListener { ForegroundService.Pause() }
        resume_button.setOnClickListener { ForegroundService.Resume() }
        next_button.setOnClickListener { ForegroundService.NextMusic() }

        applyNewMusicsList()
        refreshStatus()
        refreshProgress()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        // Add action to search view
        val searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        searchView.queryHint = "Search a music..."

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    applyFilter(newText)
                    return true
                }

                return false
            }

        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            // Refresh the list of musics
            R.id.action_refresh_list ->
                refreshMusicsList()

            // Settings activity
            R.id.action_settings ->
                startActivity(Intent(this, SetupActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }

    private fun refreshMusicsList() {
        refreshListMusics(config, object : GetListCallback {
            override fun onGotList() {
                applyNewMusicsList()
                Toast.makeText(this@MainActivity, R.string.success_refresh_list, LENGTH_SHORT)
                    .show()
            }

            override fun onError() {
                Toast.makeText(this@MainActivity, R.string.err_refresh_list, LENGTH_SHORT).show()
            }
        })
    }

    private fun applyNewMusicsList() {
        currList = getCachedList(config)
        adapter.list = currList
        adapter.notifyDataSetChanged()
    }

    private fun applyFilter(query: String) {
        adapter.list = currList.filter { music -> music.fullName.contains(query, true) }
        adapter.notifyDataSetChanged()
    }

    private fun play(music: MusicEntry?) {


        if (currList.isEmpty()) {
            Toast.makeText(this, R.string.err_no_music, Toast.LENGTH_LONG).show()
            return
        }


        val entry = when (music) {
            null -> currList[0]
            else -> music
        }

        if (playerStatus == ForegroundService.NewState.IDLE) {

            val serviceIntent = Intent(this, ForegroundService::class.java)
            serviceIntent.putExtra(
                ForegroundService.EXTRA_MUSIC_ENTRY,
                entry.serialize().toString()
            )
            ContextCompat.startForegroundService(this, serviceIntent)

        } else
            ForegroundService.ChangeMusic(entry)

    }

    override fun onMusicClick(m: MusicEntry) {
        play(m)
    }

    private fun refreshStatus() {
        play_button.visibility =
            if (playerStatus == ForegroundService.NewState.IDLE) View.VISIBLE else View.GONE

        music_name.visibility =
            if (playerStatus == ForegroundService.NewState.IDLE) View.GONE else View.VISIBLE

        musicProgress.visibility =
            if (playerStatus == ForegroundService.NewState.IDLE) View.GONE else View.VISIBLE

        next_button.visibility =
            if (playerStatus == ForegroundService.NewState.IDLE) View.GONE else View.VISIBLE

        ForegroundService.CurrEntry()?.apply {
            music_name.text = fullName
        }

        pause_button.visibility =
            if (playerStatus == ForegroundService.NewState.PLAYING ||
                playerStatus == ForegroundService.NewState.DOWNLOADING
            ) View.VISIBLE else View.GONE

        resume_button.visibility =
            if (playerStatus == ForegroundService.NewState.PAUSED) View.VISIBLE else View.GONE

    }


    private fun refreshProgress() {
        Handler().postDelayed(
            {
                if (!isDestroyed) {
                    musicProgress.progress = ForegroundService.CurrProgress()
                    refreshProgress()
                }
            }, 1000
        )
    }
}
