package org.communiquons.musicsplayer.activities

import androidx.appcompat.app.AppCompatActivity
import org.communiquons.musicsplayer.Config
import org.communiquons.musicsplayer.getConfig

abstract class BaseActivity : AppCompatActivity() {

    protected val config: Config by lazy {
        getConfig(this)
    }

}